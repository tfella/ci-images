#/usr/bin/env python3

import glob
import sys
import os
from xml.etree import ElementTree as ET

if len(sys.argv) != 2:
    print("Usage: 'get-apk-args path/to/repository'.", file=sys.stderr)
    exit(1)

path = sys.argv[1]

if not os.path.exists(path):
    print("The provided folder does not exist.", file=sys.stderr)
    sys.exit(1)

os.chdir(path)

targets = set()
directories = set()

files = glob.iglob("**/AndroidManifest.xml*", recursive=True)

manifestFound = False
for file in files:

    # third-party libraries might contain AndroidManifests which we are not interested in
    if "3rdparty" in file:
        continue

    manifestFound = True
    tree = ET.parse(file)

    prefix = '{http://schemas.android.com/apk/res/android}'
    metaDataFound = False
    for md in tree.findall("application/activity/meta-data"):
        if md.attrib[prefix + 'name'] == 'android.app.lib_name':
            metaDataFound = True
            targetName = md.attrib[prefix + 'value']
            if not targetName in targets:
                targets.add(targetName)
                directories.add(os.path.join(path, os.path.dirname(file)))
    if not metaDataFound:
        # AAR libraries can contain AndroidManifests as well, without an android.app.lib_name entry
        print("No 'android.app.lib_name' meta-data found in '{}'.".format(file), file=sys.stderr)
        continue

if not manifestFound:
    print("No AndroidManifest.xml found.", file=sys.stderr)
    exit(1)

args = "-DQTANDROID_EXPORTED_TARGET={} -DANDROID_APK_DIR={}".format(";".join(targets), ";".join(directories))
print(args)
