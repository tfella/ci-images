#escape=`
FROM kdeorg/windows-msvc2019

LABEL Description="Windows imaage for use in KDE CI Builds"
MAINTAINER KDE Sysadmin <sysadmin@kde.org>

# Install a couple of Python bindings needed by the CI Tooling
RUN pip install lxml pyyaml python-gitlab packaging; `
    Remove-Item @( 'C:\Windows\Temp\*', 'C:\Windows\Prefetch\*', 'C:\Documents and Settings\*\Local Settings\temp\*', 'C:\Users\*\Appdata\Local\Temp\*' ) -Force -Recurse -Verbose;

# Add in some items we will be passing to Craft
ADD CI-Craft-Deploy-Config.ini  CI-Craft-Packages.list "C:/Craft/"

# Switch to a cmd.exe shell
# This is needed to allow us to error out easily enough, as Powershell does not consider non-zero exit codes of commands to be fatal errors
SHELL ["cmd.exe", "/S", "/C"]

# Perform the Craft installation needed for the CI system to operate correctly
RUN `
    # Start by installing MSys2 - which Craft needs for building some dependencies
    # As only Craft uses this it will be purged at the end of this run to keep the image size down
    choco feature disable --name=showDownloadProgress && `
    choco install -y msys2 --params "/NoUpdate /InstallDir:C:\MSys2" `
    `
    # Download Craftmaster and the configuration we will be using for it...
    && git clone https://invent.kde.org/packaging/craftmaster "C:/Craft/craftmaster/" `
    && git clone https://invent.kde.org/sysadmin/binary-factory-tooling "C:/Craft/bf-tooling/" `
    `
    # Now run Craftmaster itself to build up our environment
    && cd "C:/Craft/" `
    && python craftmaster/Craftmaster.py --config bf-tooling/craft/configs/master/CraftBinaryCache.ini --config-override CI-Craft-Deploy-Config.ini --target=windows-msvc2019_64-cl -c -i --no-cache craft `
    && python craftmaster/Craftmaster.py --config bf-tooling/craft/configs/master/CraftBinaryCache.ini --config-override CI-Craft-Deploy-Config.ini --target=windows-msvc2019_64-cl -c --list-file CI-Craft-Packages.list `
    `
    # Finally do some cleanup...
    && powershell -Command "Remove-Item @( 'C:\Craft\downloads\', 'C:\Craft\windows-*\build\', 'C:\MSys2\' ) -Force -Recurse" `
    && powershell -Command "Remove-Item @( 'C:\*Recycle.Bin\S-*' ) -Force -Recurse -Verbose" `
    && powershell -Command "Remove-Item @( 'C:\Windows\Temp\*', 'C:\Windows\Prefetch\*', 'C:\Documents and Settings\*\Local Settings\temp\*', 'C:\Users\*\Appdata\Local\Temp\*' ) -Force -Recurse -Verbose"

# Disable symlinks in Git as that causes issues with breeze-icons
RUN git config --system core.symlinks false

# Finally change the shell back to Powershell
SHELL ["pwsh", "-Command", "$ErrorActionPreference = 'Stop'; $ProgressPreference = 'SilentlyContinue';"]
